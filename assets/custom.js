$(document).ready(function() {
  var offset = 0;
  if($(window).innerWidth() <= 900 ){
    $(window).scroll(function(){
      offset = $(this).scrollTop();
      var cart_btn_c_pos = $("#lp-back-button").offset().top - $(window).innerHeight();
      var pp_btn_c_pos = $("#buy_now_button_pp").offset().top + $("#buy_now_button_pp").height();
      if(offset >= cart_btn_c_pos && offset < pp_btn_c_pos){
        $("#sticky-cart-btn-mobile").css('display', 'none');
      } else {
        $("#sticky-cart-btn-mobile").css('display', 'block');
      }
    });
  }
});